### 彩信API文档



####  概述

- 本文档为彩信HTTP API调用文档，定义了提交和获取状态报告的操作
- 接口地址：`http://ip[:port]`，带请求体时，接受数据格式均为`Content-Type: appication/json`,使用`UTF-8`编码，返回数据均为`json` 。所有正常处理的请求HTTP状态码一律为`200`，还请注意。具体接口地址和账号密码咨询客服。
- 当前版本 `1.1.0`





#### 下行提交

> 此种方式为彩信群发，**无需注册模板和打包`SMIL`**，API更简易

- HTTP路由
  
   ```
   POST http://ip[:port]/{ver}/mms
   ```
  
- 提交请求

   - 提交示例
      ```
      // data.txt
      {
   	 	"account": 415411,
   	 	"password": "abcdef",
   	 	"mobile": "13311112222,13911112222",
   	 	"title": "test title",
   	 	"content": "2,jpg,base64 data,txt,dGhpcyBpcyBhIGV4YW1wbGU=|2,gif,base64data,txt,ZXhhbXBsZTI="
	   }
	   
	   // curl ip:port/v1/mms -X POST -d "@data.txt"
      ```
      
   - 参数说明

      - `accout` 系统分配的账号
      
      - `password` 系统分配的API密匙，注意，此密码和用于登录的密码不同
      
      - `mobile` 目标电话号码，用英文半角逗号`,`分隔
      
      - `title`  彩信标题
      
      - `content` 彩信内容，格式见下面具体说明:

        帧与帧之间使用`|`分隔，帧内使用英文半角逗号分隔`,`，下面以一段帧内数据为例：
        
        ```
        2,jpg,base64data,txt,dGhpcyBpcyBhIGV4YW1wbGU=
        ```
        
        每帧开头的数字标识该帧在手机上播放时显示的时长，单位s，以上示例为播放2秒
        
        `jpg`为数据类型，目前仅支持`jpg gif txt`三种类型，后续会支持音频格式
        
        `base64data`为图片数据，实际为读文件二进制数据后使用`BASE64`编码，注意若使用`Javascript btoa()`函数编码，并不包含数据头`data:MIMEType;base64,`
        
        `txt`为帧内下一段数据的类型，此后的`dGhpcyBpcyBhIGV4YW1wbGU=`为`UTF-8`文本转`BASE64`的数据
        
        同一帧内至多一张图片+一段文字，若彩信不止一帧，则使用`|`结尾开始下一帧

- 返回数据

   - 返回示例
   
   ```
   {
	"code": 0,
	"message": "OK",
	"data": {
		"list": [{
			"mobile": "13311112222",
			"code": 0,
			"uuid": "6a37183c-c083-4c84-a8e9-b784e40a77a4"
		},{
			"mobile": "13911112222",
			"code": 0,
			"uuid": "8a37ag3c-h083-i989-0604-b7zre50a77b4"
		}]
	}
	}
	```

   - 参数说明
   
      - `code` 和`message`  返回码和信息，参见附录
      - `data.list` 每条号码提交的具体情况，当`data.list[n].code = 0`时该条号码提交成功，其余失败,`data.list[n].mobile`为对应的电话号码，`data.list[n].uuid`为该条彩信的UUID，查询状态报告时用于对照
   







#### 注册模板

- HTTP路由 

  ```
  POST http://ip[:port]/{ver}/tpl			// 注册模板
  ```
  

  
- 提交请求

  - 提交示例
  
  ```
  // 注册模板示例
  // data.txt
  {
   	"account": 415411,
   	"password": "abcdef",
   	"template": "dGVzdCB0ZW1wbGF0ZSB7e3JlcGxhY2VLZXl9fQ=="
  }
  
  // curl ip:port/v1/tpl -X POST -d "@data.txt"
  ```
  
  - 参数说明
    
    - `account` 系统分配的账号
    
    - `password` 系统分配的API密匙，注意，此密码和用于登录的密码不同
    
    - `template` 模板内容，原始文件经 `ZIPEncode` 再做 `BASE64Encode` 排版格式见以下说明
    
      1. 图片替换
    
         图片变量只能存在于`SMIL`文件中，变量格式为`${N}$` ,`N`为从`0`开始的整数，例如`${0}$  ${1}$ ...`。下面以一个`SMIL`文件示例说明:
    
         ```
         <smil>
         <head>
         <layout>
         <region id="regImg" width="100%" height="50%" left="0%" top="0%" fit="meet" />
         <region id="regTxt" width="100%" height="50%" left="0%" top="50%" fit="meet" />
         </layout>
         </head>
         <body>
         <par dur="4s">
         <img src="${0}$" region="regImg" />
         <text src="001.txt" region="regTxt" />
         </par>
         
         <par dur="4s">
         <img src="${1}$" region="regImg" />
         <text src="002.txt" region="regTxt" />
         </par>
         
         <par dur="4s">
         <img src="static.jpg" region="regImg" />
         <text src="003.txt" region="regTxt" />
         </par>
         </body>
         </smil>
         ```
         
         以上实例中，将该`SMIL`文件和`001.txt 002.txt 003.txt static.jpg`一起经过`ZIPEncode` -> `BASE64Encode`后，设为参数`template`的值即可
         
         > 请不要在`SMIL`文件中在其他地方使用模板符号`${N}$`，仅能用于标签`<img> src`属性。直接指定文件名可以实现静态提交
      
      2. 文本替换
      
         ```001.txt
         // 接上面示例，假设001.txt中存在变量需要替换，这里是001.txt的文本示例
         
         【SIGN】用户${2}$您好，您的验证码是${3}$
         ```
         
         > 每个文本中的**变量N全局唯一**，即模板中SMIL和TXT文件中的N应当是从0递增
      
  
- 返回数据

  - 返回示例

    ```
    {
     	"code": 0,
     	"message": "OK",
     	"data": {
     	 	"templateID": "16297122-11c3-45df-a3af-1f49a83ae4b5"
     	}
    }
    ```

  - 参数说明
  
    - `code`和`message` - 返回码和信息，参见附录
    - `templateID` 模板ID，用于`模板调用`部分





#### 查询模板

- HTTP路由

  ```
  GET  http://ip[:port]/{ver}/tpl	
  ```

- 提交请求

  - 提交示例

    ```
    // 查询模板示例
    curl 'ip:port/v1/tpl?account=415411&password=123456&templateID=16297122-11c3-45df-a3af-1f49a83ae4b5'
    ```

  - 参数说明

    - `account` `password` 账号密码
    - `templateID` 模板ID

- 返回数据

  - 返回示例

    ```
    {
     	"code": 0,
     	"message": "OK",
     	"data": {
     		"status": 0,
     		"content": "6CB44CR5oKo55qE6aqM6K+B56C"
     	}
    }
    ```
    
  - 参数说明
  
    - `code` 和 `message` 返回码和信息，参见附录 
    - `data.status` - 模板是否通过审核已可用
      - 0 - 未审核，不可用
      - 1 - 已审核，可用
      - 2 - 审核驳回，不可用 
    - `data.content` - 模板原始数据





#### 模板调用

- HTTP路由
  ```
  POST http://ip[:port]/{ver}/tpl/mms
  ```

- 提交请求

  - 提交示例

  ```
    // data.txt
    {
     	"account": 415411,
     	"password": "abcdef",
     	"title": "template submit title",
     	"templateID": "16297122-11c3-45df-a3af-1f49a83ae4b5",
     	"attachment": "CR5oKo55qE6aqM6K+B56",
     	"mobile": {
     			"13311112222": ["133.jpg" ,"133_2.jpg" ,"zhangsan"...],
     			"14411112222": ["144.jpg" ,"144_2.jpg" ,"lisi"...],
     			"15511112222": ["155.jpg" ,"155_2.jpg" ,"wangwu"...],
     			...
     			...
     	}
    }
    
    // curl ip:port/v1/tpl/mms -X POST -d "@data.txt"
  ```

  - 参数说明

    - `account` `password` 账号密码
    - `title` - 彩信标题
    - `templateID` - 模板ID
    - `attachment` - 附件经ZIPEncode -> BASE64后的数据
    - `mobile` - 电话号码和替换参数的键值对，以`"13311112222": ["133.jpg" ,"133_2.jpg" ,"zhangsan"...]`为例  ,`133.jpg`将替换`${0}$`,  `133_2.jpg`将替换`${1}$`， `zhangsan`将替换`${2}$`，以此类推，`133.jpg`等文件应当在`attachment`被解析后找到。若是静态彩信，不需要修改变量，则参数部分为空数组即可，例如`"13311112222:[]"`
    
    

- 返回数据

  - 返回示例

  ```
    {
    	"code": 0,
    	"message": "OK",
    	"data": {
    		"list": [{
    			"mobile": "13311112222",
    			"code": 0,
    			"uuid": "6a37183c-c083-4c84-a8e9-b784e40a77a4"
    		},{
    			"mobile": "13911112222",
    			"code": 0,
    			"uuid": "8a37ag3c-h083-i989-0604-b7zre50a77b4"
    		}]
    	}
    }
  ```

  > 调用模板下行返回数据与普通下行返回相同，还请参照**下行提交 - 返回数据 **部分



#### 状态报告

- HTTP路由 
  ```
  GET http://ip[:port]/{ver}/mms
  ```

- 提交请求

  - 提交示例

  ```
    // "account=?&password=?"
    
    curl "ip:port/v1/mms?account=415411&password=abcdef"
  ```

  - 参数说明

    - `account` 系统分配的账号
    - `password` 系统分配的API密匙，注意，此密码和用于登录的密码不同

- 返回数据

  - 返回示例 

    ```
    {
    	"code": 0,
    	"message": "OK",
    	"data": [{
    		"status": 0,
    		"mobile": "13311112222",
    		"uuid": "6a37183c-c083-4c84-a8e9-b784e40a77a4"
    	},{
    		"status": 0,
    		"mobile": "13911112222",
    		"uuid": "8a37ag3c-h083-i989-0604-b7zre50a77b4"
    	}]
    }
    ```

  - 参数说明

    - `code` 和 `message` 返回码和信息，参见附录
    - `data[n].status`
      - 0 - 成功
      - 其他 - 失败
    - `data[n].uuid`
      - 标识此条彩信的UUID，与提交返回中的`data.list[n].uuid`对应



<h4 id="att">附录</h4>
- 返回状态码和信息

  | 返回码 code | 信息 msg       |
  | ----------- | -------------- |
	| 0           | 成功           |
	| 1           | 非法账户       |
	| 2           | 账号未开通彩信 |
	| 3			  | 余额不足 |
	| 4 | 参数错误 |
	| 5 | 服务器错误 |
	| 6 | IP鉴权错误 |
	| 7 | 彩信标题为空 |
	| 8 | 提交号码为空 |
	| 9 | 提交内容为空 |
	| 10 | 内容大小超过限制，为最大为64kb |
	| 11 | 单次提交的号码过多，建议小于2000条 |
	| 12 | 帧数量过多，最大限制为10帧 |
	| 13 | 提交模板内容为空 |
	| 14 | 找不到指定模板，或指定模板不可用 |
	



#### 修订记录

- 2019-11-04  - 增加了提交返回码`10,11`
- 2019-11-06 - 增加了**模板调用 模板注册**API

